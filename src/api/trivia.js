const BASE_URL = 'https://opentdb.com';

/**
 * Fetches quetsions from opentdb.com and returns them as a list.
 *
 * @param {String} difficulty - can be "easy", "medium" or "hard"
 * @param {Number} numberOfQuestions - specifies the length of the result list
 * @param {Number} category - category id number
 * @returns {Array} - a list of question objects
 */
export const getQuestionByCategory = (
  difficulty,
  numberOfQuestions,
  category
) => {
  return fetch(
    `${BASE_URL}/api.php?amount=${numberOfQuestions}&category=${category}&difficulty=${difficulty}`
  )
    .then((response) => response.json())
    .then((res) => res.results);
};

/**
 * Fetches the specified amount of question, and difficulty with different categories
 *
 * @param {String} difficulty - can be "easy", "medium" or "hard"
 * @param {Number} numberOfQuestions - specifies the length of the result list
 * @returns {Array} - a list of question objects
 */
export const getQuestionWithAnyCategory = (difficulty, numberOfQuestions) => {
  return fetch(
    `${BASE_URL}/api.php?amount=${numberOfQuestions}&difficulty=${difficulty}`
  )
    .then((response) => response.json())
    .then((data) => data.results);
};

/**
 * Fetches all the categories from the opentd.com.
 * @returns {Array} - a list of different categories objects.
 */
export const getAllCategories = () => {
  return fetch(`${BASE_URL}/api_category.php`)
    .then((response) => response.json())
    .then((categoryData) => categoryData.trivia_categories);
};
