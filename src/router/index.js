import Vue from 'vue';
import VueRouter from 'vue-router';
import StartPage from '../views/StartPage';
import GameRound from '../views/GameRound';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'StartPage',
    component: StartPage,
  },
  { path: '/game', name: 'game', component: GameRound },
];

const router = new VueRouter({
  routes,
});

export default router;
